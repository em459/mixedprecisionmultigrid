#ifndef PRECONDITIONER_HH
#define PRECONDITIONER_HH PRECONDITIONER_HH
#include "fieldvector.hh"
#include "operator.hh"
#include "grid.hh"

/* ************************************************************* *
   Abstract base class for preconditioner which implements y = P.x

   This derives from the abstract operator base class. The
   preconditioner acts on fieldvectors with internal storage
   type T.
 * ************************************************************* */
 
template<typename T>
class Preconditioner : public Operator<T> {
public:
  typedef T RealType;
  /* constructor */
  Preconditioner(const Grid& grid_) : Operator<T>(grid_) {};
};

/* ************************************************************* *
   Class for null-preconditioner y = x
 * ************************************************************* */
template<typename T>
class NullPreconditioner : public Preconditioner<T> {
public:
  typedef T RealType;
  /* constructor */
  NullPreconditioner(const Grid& grid_) : Preconditioner<T>(grid_) {}
private:
  /* implementation of y = x */
  virtual void apply_engine(const FieldVector<T>& x, FieldVector<T>& y) {
    std::copy(x.data,x.data+Preconditioner<T>::n,y.data);
  }
};

/* ************************************************************* *
   Class for matrix-diagonal preconditioner y = D^{-1}.x

   Given a dense matrix operator A, this preconditioner implements
   the operation y = D^{-1}.x where D is the diagonal of A. This
   diagonal is stored as a vector of type Tdiag.
 * ************************************************************* */
template<typename T, typename Tdiag >
class DiagonalPreconditioner : public Preconditioner<T> {
public:
  typedef T RealType;
  /* constructor */
  template<typename Top, typename Tmat>
  DiagonalPreconditioner(const Grid& grid_,
                         const DenseMatrixOperator<Top,Tmat>& matrix_operator_) :
    Preconditioner<T>(grid_) {
    // allocate memory
    inv_diag_data = new Tdiag[Preconditioner<T>::n];
    // assign entries of inverse diagonal
    for (unsigned int i=0;i<Preconditioner<T>::n;++i) {
      inv_diag_data[i] = 1.0/matrix_operator_.get(i,i);
    }
  }

  template<typename Top, typename Tmat>
  DiagonalPreconditioner(const Grid& grid_,
                         const HelmholtzOperator<Top,Tmat>& helmholtz_operator_) :
    Preconditioner<T>(grid_) {
    // allocate memory
    inv_diag_data = new Tdiag[Preconditioner<T>::n];
    // assign entries of inverse diagonal
    for (unsigned int i=0;i<Preconditioner<T>::n;++i) {
      inv_diag_data[i] = 1.0/helmholtz_operator_.get_diag(i);
    }
  }

  /* destructor. free internal storage of inverse diagonal */
  ~DiagonalPreconditioner() {
    delete[] inv_diag_data;
  }

  /* get inverse diagonal entry at position i */
  const Tdiag get(unsigned int i) const {
    return inv_diag_data[i];
  }

private:
  /* implementation of the operator y = D^{-1}.A */
  virtual void apply_engine(const FieldVector<T>& x, FieldVector<T>& y) {
    std::transform(inv_diag_data,inv_diag_data+Preconditioner<T>::n,x.data,y.data,
                   [](const Tdiag a, const T b){ return a*b; });
  }

protected:
  Tdiag *inv_diag_data; // Inverse diagonal of the matrix
};

/* Write inverse diagonal to an output stream */
template <typename T, typename Tdiag> 
std::ostream& operator<<(std::ostream& os, const DiagonalPreconditioner<T,Tdiag>& P)
{
  unsigned int n = P.size();
  os << "[";
  for (int i = 0; i < n; ++i) { 
    os << P.get(i);
    if ((i+1) != n) { 
      os << ", ";
    }
  }
  os << "]"; 
  return os; 
}


#endif // PRECONDITIONER_HH
