#include <iostream>
#include <algorithm>
#include <math.h>

/* Diffusion function */
double K(double x, double y) { return 1.0; }

/* two-norm */
double two_norm(int nx, int ny, const double* u) {
  double nrm2 = 0.0;
  for (int ell=0;ell<nx*ny;++ell) {
    nrm2 += u[ell]*u[ell];
  }
  return sqrt(nrm2);
}

/* Create matrix */
void createH(const int nx,
             const int ny,
             const double hx,
             const double hy,
             const double alpha,
             const double beta,
             double* H_d,
             double* H_n,
             double* H_s,
             double* H_e,
             double* H_w) {
  for (int i=0;i<nx;++i) {
    for (int j=0;j<ny;++j) {
      double xi = (i+0.5)*hx;
      double yj = (j+0.5)*hy;
      int ell = i+j*nx;
      H_n[ell] = -alpha*hx/hy*K(xi,yj+0.5*hy);
      H_s[ell] = -alpha*hx/hy*K(xi,yj-0.5*hy);
      H_e[ell] = -alpha*hy/hx*K(xi+0.5*hx,yj);
      H_w[ell] = -alpha*hy/hx*K(xi-0.5*hx,yj);
      H_d[ell] = beta*hx*hy - H_n[ell] - H_s[ell] - H_e[ell] - H_w[ell];
    }
  }
}

/* Calculate v = H.u */
void applyH(const int nx,
            const int ny,
            const double* H_d,
            const double* H_n,
            const double* H_s,
            const double* H_e,
            const double* H_w,
            const double* u,
            double* v) {
  int N = nx*ny;
  for (int ell=0;ell<N;++ell) {
    v[ell] = H_d[ell]*u[ell];
    v[ell] += H_n[ell]*u[(ell+nx)%N];
    v[ell] += H_s[ell]*u[(ell-nx+N)%N];
    v[ell] += H_e[ell]*u[(ell+1)%N];
    v[ell] += H_w[ell]*u[(ell-1+N)%N];
  }
}

/* Apply niter Jacobi iterations */
void jacobi(const int nx,
            const int ny,
            const double* H_d,
            const double* H_n,
            const double* H_s,
            const double* H_e,
            const double* H_w,
            const int niter,
            const double* b,
            double* u) {
  int N=nx*ny;
  double *Hu = new double[N];
  for (int k=0;k<niter;++k) {
    applyH(nx,ny,H_d,H_n,H_s,H_e,H_w,u,Hu);
    for (int ell=0;ell<N;++ell) {
      u[ell] = u[ell] + (b[ell] - Hu[ell])/H_d[ell];
    }
    {
      applyH(nx,ny,H_d,H_n,H_s,H_e,H_w,u,Hu);
      for (int ell=0;ell<N;++ell) {
        Hu[ell] -= b[ell];
      }
      std::cout << " " << k << " : " << two_norm(nx,ny,Hu) << std::endl;
    }
  }
  delete[] Hu;
}

int main(int argc, char* argv[]) {
  /* Size of domain */
  const double Lx = 1.0;
  const double Ly = 1.0;
  /* Problem size */
  const int nx=16;
  const int ny=16;
  const int N = nx*ny;
  /* Grid spacing */
  const double hx = Lx/nx;
  const double hy = Ly/ny;
  /* Constants alpha and beta */
  const double alpha = 1.0;
  const double beta = 1.0;
  /* Number of Jacobi iterations */
  const int niter = 1000;
  /* Print out problem parameters */
  std::cout << " nx = " << nx << std::endl;
  std::cout << " ny = " << ny << std::endl;
  std::cout << " hx = " << hx << std::endl;
  std::cout << " hy = " << hy << std::endl;
  /* Exact solution */
  double* u_exact = new double[N];
  /* Fill with random numbers */
  srand(541956197);
  for (int ell=0;ell<N;++ell) {
    u_exact[ell] = 1.0+0.001*(rand() % 100);
  }
  
  /* Approximate solution */
  double* u = new double[N];
  /* Helmholtz matrix */
  double* H_d = new double[N];
  double* H_n = new double[N];
  double* H_s = new double[N];
  double* H_e = new double[N];
  double* H_w = new double[N];
  createH(nx,ny,hx,hy,alpha,beta,H_d,H_n,H_s,H_e,H_w);

  /* Right hand side */
  double* b = new double[N];
  applyH(nx,ny,H_d,H_n,H_s,H_e,H_w,u_exact,b);

  /* Apply Jacobi iterations */
  std::fill(u,u+N,0.0);
  jacobi(nx,ny,H_d,H_n,H_s,H_e,H_w,niter,b,u);

  /* Deallocate all memory */
  delete[] u_exact;
  delete[] u;
  delete[] H_d;
  delete[] H_n;
  delete[] H_s;
  delete[] H_e;
  delete[] H_w;
  delete[] b;
}
