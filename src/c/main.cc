#include <iostream>
#include <random>
#include <algorithm>
#include "fieldvector.hh"
#include "operator.hh"
#include "preconditioner.hh"
#include "solver.hh"
#include "grid.hh"
#include "config.hh"

int main(int argc, char* argv[]) {
  std::cout << "+------------------------+" << std::endl;
  std::cout << "! Mixed precision solver !" << std::endl;
  std::cout << "+------------------------+" << std::endl;

  std::cout << std::endl;
  std::cout << " nx = " << nx << std::endl;
  std::cout << " ny = " << ny << std::endl;
  std::cout << std::endl;
  /* create a grid */
  Grid grid(nx,ny,Lx,Ly);

  /* define vectors */
  FieldVector<T> u_exact(grid); // exact solution
  FieldVector<T> u(grid); // numerical solution
  FieldVector<T> b(grid); // right hand side  
  u_exact[1] = 1.0; // set to one

  /* Create dense random matrix */
#ifdef MATRIX_OPERATOR
  std::mt19937 generator(641846197);
  std::uniform_real_distribution<> uniform_dist(0.0, 1.0);
  unsigned int n = grid.size();
  Tmat* mat_data = new Tmat[n*n];
  for (unsigned int i=0;i<n;++i) {
    mat_data[(n+1)*i] = 1.0;
    for (unsigned int j=i;j<n;++j) {
      Tmat tmp = (1./((Tmat)n))*uniform_dist(generator);
      mat_data[i*n+j] += tmp;
      mat_data[j*n+i] += tmp;
    }
  }
  typedef DenseMatrixOperator<Top,Tmat> OperatorType;
  OperatorType A(grid,mat_data);  
  delete [] mat_data;
#endif // MATRIX_OPERATOR

#ifdef HELMHOLTZ_OPERATOR
  typedef HelmholtzOperator<Top,Tmat> OperatorType;
  OperatorType A(grid,1.0,[](std::pair<double,double> p) {return 1.0+0.1*p.first*p.first-0.2*p.second*p.second;});
#endif // HELMHOLTZ_OPERATOR
  
  /* Create null-preconditioner or diagonal preconditioner
     based on this matrix */
#ifdef NULL_PRECONDITIONER
  std::cout << "preconditioner = null" << std::endl;
  typedef NullPreconditioner<Tprec> PreconditionerType;
  PreconditionerType P(grid);
#endif // DIAGONAL_PRECONDITIONER

#ifdef DIAGONAL_PRECONDITIONER
  std::cout << "preconditioner = diagonal" << std::endl;
  typedef DiagonalPreconditioner<Tprec,Tdiagmat> PreconditionerType;
  PreconditionerType P(grid,A);
#endif // DIAGONAL_PRECONDITIONER

  /* Create CG solver */
  CGSolver<T,OperatorType,PreconditionerType> solver(grid,A,P);
  solver.set_maxiter(1000);
  solver.set_tolerance(1.E-8);

  /* Construct RHS b = A.u_exact. This ensures that the numerical solution
     will converge to u_exact */
  A.apply(u_exact,b);
  solver.apply(b,u);

  /* Output two-norm of error */
  std::cout << "||u-u_exact|| = " << (u_exact-u).two_norm() << std::endl;
}
