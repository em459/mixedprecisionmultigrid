#ifndef CONFIG_HH
#define CONFIG_HH CONFIG_HH
/* *** definitions *** */

/* type of field vector used in iterative solver */
typedef double T;
/* type of field vector used in the linear operator */
typedef double Top;      // operator type
/* type of field vector used in the preconditioner */
typedef float Tprec;    // preconditioner type
/* type of internal storage used for matrix operator */
typedef double Tmat;     // matrix storage type
/* type of internal storage used for diagonal preconditioner */
typedef float Tdiagmat; // diagonal matrix storage type

/* grid size and number of cells in x- and y-direction */
const double Lx = 1.0;
const double Ly = 1.0;
const unsigned int nx=32;
const unsigned int ny=32;

/* operator */
#define HELMHOLTZ_OPERATOR
//#define MATRIX_OPERATOR
/* preconditioner */
#define DIAGONAL_PRECONDITIONER
//#define NULL_PRECONDITIONER

/* Linear mapping */
#define LINIDX(NX,NY,i,j) ((nx)*(j)+(i))

#endif // CONFIG_HH
