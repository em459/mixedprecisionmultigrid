#ifndef OPERATOR_HH
#define OPERATOR_HH OPERATOR_HH
#include <typeinfo>
#include <iostream>
#include <functional>
#include "config.hh"
#include "grid.hh"
#include "fieldvector.hh"

/* ************************************************************* *
   Abstract base class for linear operators which implement y = A.x

   The template argument T is the internal storage type on which 
   the operator acts. All derived classes have to implement the
   private method apply_engine(x,y) which calculates the y = A.x for
   x, y of type FieldVector<T>.
   However, the public apply(x,y) method can also take FieldVector's
   with a different storage type Tapply. If Tapply differs from T, data
   is copied before and after calling apply_engine(). If necessary,
   temporary storage is allocated in the first call to apply().
 * ************************************************************** */
template<typename T>
class Operator {
public:
  /* constructor */
  Operator(const Grid& grid_) : grid(grid_), n(grid_.size()) {}

  /* destructor. free temporary storage, if this has been allocated */
  ~Operator() {
    if (x_double != NULL) delete x_double;
    if (y_double != NULL) delete y_double;
    if (x_float != NULL) delete x_float;
    if (y_float != NULL) delete y_float;
  }
  
  /* Public interface for operator application: y = A.x */
  template <typename Tapply>
  void apply(const FieldVector<Tapply>& x, FieldVector<Tapply>& y) {
    apply_engine(x,y);
  }

  /* return number of grid cells */
  const unsigned int size() const { return n; }

private:
  /* virtual apply engine method. Has to be implemented in every
     derived class */
  virtual void apply_engine(const FieldVector<T>& x, FieldVector<T>& y) = 0;
  
protected:
  const Grid& grid; // Underlying grid
  const unsigned int n; // Number of grid cells
  // temporary storage
  FieldVector<double>* x_double=NULL;
  FieldVector<double>* y_double=NULL;
  FieldVector<float>* x_float=NULL;
  FieldVector<float>* y_float=NULL;
};

/* Template specification of apply() interface for case 
   T = double, Tapply = float. */
template<> template<>
void Operator<double>::apply<float>(const FieldVector<float>& x,
                                    FieldVector<float>& y) {
  // if necessary, allocate temporary storage
  if (x_double == NULL) x_double = new FieldVector<double> (grid);
  if (y_double == NULL) y_double = new FieldVector<double> (grid);
  // Copy from input vector x
  *x_double = const_cast<FieldVector<float>&>(x);
  // Call to engine
  apply_engine(*x_double,*y_double);
  // Copy to output vector y
  y = *y_double;
}

/* Template specification of apply() interface for case 
   T = float, Tapply = double. */
template<> template<>
void Operator<float>::apply<double>(const FieldVector<double>& x,
                                    FieldVector<double>& y) {
  // if necessary, allocate temporary storage
  if (x_float == NULL) x_float = new FieldVector<float> (grid);
  if (y_float == NULL) y_float = new FieldVector<float> (grid);
  // Copy from input vector x
  *x_float = const_cast<FieldVector<double>&>(x);
  // Call to engine
  apply_engine(*x_float,*y_float);
  // Copy to output vector y
  y = *y_float;
}

/* ************************************************************* *
   Class storing a dense operator

   The operator acts on FieldVectors with interal storage type T,
   but the matrix is stored internally as an array of type Tmat.
   This class is derived from the abstract base class Operator<T>.
 * ************************************************************* */
template<typename T, typename Tmat>
class DenseMatrixOperator : public Operator<T> {
public:
  typedef T RealType;
  typedef Tmat MatRealType;
  /* constructor */
  DenseMatrixOperator(const Grid& grid_,
                      const Tmat* data_) : Operator<T>(grid_) {
    data = new Tmat[Operator<T>::n*Operator<T>::n];
    std::copy(data_,data_+Operator<T>::n*Operator<T>::n,data);
  }

  /* destructor. Free internal matrix storage */
  ~DenseMatrixOperator() {
    delete[] data;
  }

  /* Get element at position (i,j) of the dense matrix */
  const Tmat get(unsigned int i, unsigned int j) const {
    return data[i*Operator<T>::n+j];
  }

private:
  /* Apply operator: dense matrix multiplication */
  virtual void apply_engine(const FieldVector<T>& x, FieldVector<T>& y) {
    for (unsigned int i=0; i<Operator<T>::n; ++i) {
      T tmp = 0.0;
      for (unsigned int j=0; j<Operator<T>::n; ++j) {
        tmp += data[i*Operator<T>::n+j]*x[j];
      }
      y[i] = tmp;
    }
  }
  
protected:
  Tmat* data; // internal storage for dense matrix

};

/* ************************************************************* *
   Class storing a Helmholtz operator

   Implements a finite-volume discretisation of the linear operator
   -alpha*div(Kdiff(x) grad(u)) + u where K(x) is a spatially varying
   field and alpha is a constant. For simplicity, periodic boundary
   conditions are assumed in both spatial directions.
   The operator acts on FieldVectors with interal storage type T,
   but the stencil is stored internally as an array of type Tmat.
   This class is derived from the abstract base class Operator<T>.
 * ************************************************************* */
template<typename T, typename Tmat>
class HelmholtzOperator : public Operator<T> {
public:
  typedef T RealType;
  typedef Tmat MatRealType;
  /* constructor */
  HelmholtzOperator(const Grid& grid_,
                    const double alpha_,
                    std::function<double(std::pair<double,double>)> Kdiff_) : Operator<T>(grid_), alpha(alpha_), Kdiff(Kdiff_) {
    H_diag = new Tmat[Operator<T>::grid.size()];
    H_east = new Tmat[Operator<T>::grid.size()];
    H_west = new Tmat[Operator<T>::grid.size()];
    H_north = new Tmat[Operator<T>::grid.size()];
    H_south = new Tmat[Operator<T>::grid.size()];
    // aspect ratio rho = hy/hx
    std::cout << "rho = " << Operator<T>::grid.get_hy() << std::endl;
        
    Tmat rho = Operator<T>::grid.get_hy()/Operator<T>::grid.get_hx();
    // volume of a grid cell hx*hy
    Tmat volume = Operator<T>::grid.get_hy()*Operator<T>::grid.get_hx();
    for (unsigned int i=0;i<Operator<T>::grid.get_nx();++i) {
      for (unsigned int j=0;j<Operator<T>::grid.get_ny();++j) {
        Tmat tmp_east = -alpha*rho*Kdiff(Operator<T>::grid.get_east_facet_center(i,j));
        Tmat tmp_west = -alpha*rho*Kdiff(Operator<T>::grid.get_west_facet_center(i,j));
        Tmat tmp_north = -alpha/rho*Kdiff(Operator<T>::grid.get_north_facet_center(i,j));
        Tmat tmp_south = -alpha/rho*Kdiff(Operator<T>::grid.get_south_facet_center(i,j));
        H_east[LINIDX(nx,ny,i,j)] = tmp_east;
        H_west[LINIDX(nx,ny,i,j)] = tmp_west;
        H_north[LINIDX(nx,ny,i,j)] = tmp_north;
        H_south[LINIDX(nx,ny,i,j)] = tmp_south;
        H_diag[LINIDX(nx,ny,i,j)] = volume - (tmp_east+tmp_west+tmp_north+tmp_south);
      }
    }
  }

  /* destructor. free allocated memory for local stencil */
  ~HelmholtzOperator() {
    delete[] H_diag;
    delete[] H_east;
    delete[] H_west;
    delete[] H_north;
    delete[] H_south;
  }

  /* get diagonal entry for a certain linear index */
  const Tmat get_diag(unsigned int ell) const {
    return H_diag[ell];
  }
  
private:
  /* Apply operator: dense matrix multiplication */
  virtual void apply_engine(const FieldVector<T>& x, FieldVector<T>& y) {
    unsigned int nx = Operator<T>::grid.get_nx();
    unsigned int ny = Operator<T>::grid.get_ny();
    for (unsigned int i=0;i<nx;++i) {
      for (unsigned int j=0;j<ny;++j) {
        unsigned i_p = (i + 1)%nx;
        unsigned i_m = (i - 1)%nx;
        unsigned j_p = (j + 1)%ny;
        unsigned j_m = (j - 1)%ny;
        T tmp = H_diag[LINIDX(nx,ny,i,j)]*x.get(i,j)
          + H_east[LINIDX(nx,ny,i,j)]*x.get(i_p,j)
          + H_west[LINIDX(nx,ny,i,j)]*x.get(i_m,j) 
          + H_north[LINIDX(nx,ny,i,j)]*x.get(i,j_p)
          + H_south[LINIDX(nx,ny,i,j)]*x.get(i,j_m);
        y.set(i,j,tmp);
      }
    }
  }
protected:
  const double alpha;
  std::function<double(std::pair<double,double>)> Kdiff;
  Tmat* H_diag;
  Tmat* H_east;
  Tmat* H_west;
  Tmat* H_north;
  Tmat* H_south;
};

/* Write a DenseMatrixOperator to an output stream */
template <typename T, typename Tmat> 
std::ostream& operator<<(std::ostream& os, const DenseMatrixOperator<T,Tmat>& A) 
{
  unsigned int n = A.size();
  os << "[";
  for (int i = 0; i < n*n; ++i) { 
    os << A.get(i/n,i%n);
    if ((i+1) != n*n) { 
      if ((i + 1) % n == 0) {
        os << "; ";
      } else {
        os << ", ";
      }
    }
  } 
  os << "]"; 
  return os; 
}


#endif // OPERATOR_HH
