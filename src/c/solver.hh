#ifndef SOLVER_HH
#define SOLVER_HH SOLVER_HH
#include "grid.hh"
#include "fieldvector.hh"
#include "operator.hh"
#include "preconditioner.hh"

/* ************************************************************* *
   Abstract base class for linear solver, which solves A.x = b

   Both the linear operator A and a preconditioner P are passed to
   the constructor. All interal field vectors used by the linear
   solver are assumed to have internal storage type T.
   The virtual apply(b,x) method has to be implemented in derived
   classes to solve the system A.x = b.
   The system is solve to a specified relative tolerance on the 
   residual r = b - A.x, i.e. until ||r||/||r_0|| < tolerance. The
   maximal number of iterations can be specified in the variable
   maxiter.
 * ************************************************************* */
template <typename T, class OperatorType, class PreconditionerType>
class Solver {
public:
  typedef typename PreconditionerType::RealType Tprec;
  typedef typename OperatorType::RealType Top;
  /* constructor */
  Solver(const Grid& grid_,
         OperatorType& op_,
         PreconditionerType& prec_) : grid(grid_), op(op_), prec(prec_), maxiter(100), tolerance(1.E-12) {}

  /* abstract method which implements the solution of A.x = b */
  virtual void apply(const FieldVector<T>& b, FieldVector<T>& x) = 0;

  /* set the solver tolerance */
  void set_tolerance(const double tolerance_) { tolerance = tolerance_; }

  /* get the solver tolerance */
  const double get_tolerance() const { return tolerance; }

  /* set the maximal number of iterations */
  void set_maxiter(const unsigned int maxiter_) { maxiter = maxiter_; }

  /* get the maximal number of iterations */
  const unsigned int get_maxiter() const { return maxiter; }

protected:
  const Grid& grid; // grid
  OperatorType& op; // linear operator A
  PreconditionerType& prec; // preconditioner P
  unsigned int maxiter; // Maximal number of iterations
  double tolerance; // tolerance for iterative solve

};

/* ************************************************************* *
   Class for preconditioned conjugate gradient (PCG) solver

   Solves the system A.x = b for symmetric positive definite
   linear operator A and preconditioner P with the preconditioned
   conjugate gradient method.
 * ************************************************************* */
template <typename T, class OperatorType, class PreconditionerType>
  class CGSolver : public Solver<T,OperatorType,PreconditionerType> {
public:
  typedef typename PreconditionerType::RealType Tprec;
  typedef typename OperatorType::RealType Top;
  typedef Solver<T,OperatorType,PreconditionerType> BaseType;
  /* constructor */
  CGSolver(const Grid& grid_,
           OperatorType& op_,
           PreconditionerType& prec_) : Solver<T,OperatorType,PreconditionerType>(grid_,op_,prec_) {
    // allocate storage for temporary vectors
    r0 = new FieldVector<T> (BaseType::grid);
    r = new FieldVector<T> (BaseType::grid);
    q = new FieldVector<T> (BaseType::grid);
    p = new FieldVector<T> (BaseType::grid);
    z = new FieldVector<T> (BaseType::grid);
  }

  /* destructor */
  ~CGSolver() {
    // free storage for temporary vectors
    delete r0;
    delete r;
    delete q;
    delete p;
    delete z;
  }

  /* method implementing the PCG method */
  virtual void apply(const FieldVector<T>& b, FieldVector<T>& x) {    
    (*r0) = const_cast<FieldVector<T>&>(b);
    BaseType::op.apply(x,*q);
    (*r0) -= (*q);
    (*r) = (*r0);
    BaseType::prec.apply(*r0,*z);
    (*p) = (*z);
    T zdotr_old = z->dot(*r);
    T r0_two_norm = r0->two_norm();
    T r_two_norm_old = r0->two_norm();
    char buffer[64];
    std::sprintf (buffer, " initial residual : %12.4e",r0_two_norm);
    std::cout << buffer << std::endl;
    std::sprintf (buffer, " %4s : %12s  %12s  %6s","iter","||r||","||r||/||r_0||","rho");
    std::cout << buffer << std::endl;
    for (int k=0;k<BaseType::maxiter;++k) {
      BaseType::op.apply(*p,*q);
      T alpha = zdotr_old/p->dot(*q);
      x.axpy(+alpha,*p);
      r->axpy(-alpha,*q);
      T r_two_norm = r->two_norm();
      std::sprintf (buffer, " %4d : %12.4e  %12.4e  %6.3f",
                    k,
                    r_two_norm,
                    r_two_norm/r0_two_norm,
                    r_two_norm/r_two_norm_old);
      std::cout << buffer << std::endl;
      if (r_two_norm/r0_two_norm < BaseType::tolerance) break;
      BaseType::prec.apply(*r,*z);
      T zdotr = z->dot(*r);
      T beta = zdotr/zdotr_old;
      p->aypx(beta,*z);
      zdotr_old = zdotr;
      r_two_norm_old = r_two_norm;
    }
  }
 
private:
  // temporary vectors used in the PCG method
  FieldVector<T>* r0;
  FieldVector<T>* r;
  FieldVector<T>* q;
  FieldVector<T>* p;
  FieldVector<T>* z;
};

#endif // SOLVER_HH
