#ifndef GRID_HH
#define GRID_HH GRID_HH
#include <utility>

/* ************************************************************* *
   Class for storing the data of a 2d-grid with nx x ny cells.

   The degrees of freedom are assumed to be arranged like this:

   12  13  14  15       N
    8   9  10  11     W   E
    4   5   6   7       S
    0   1   2   3
   
 * ************************************************************* */
class Grid {
public:
  typedef std::pair<double,double> Point;
  Grid(const unsigned int nx_, unsigned int ny_,
       const double Lx_, const double Ly_) : nx(nx_), ny(ny_), n(nx_*ny_), Lx(Lx_), Ly(Ly_), hx(Lx_/nx_), hy(Ly_/ny_) {}

  /* return number of cells in x-direction */
  const unsigned int get_nx() const { return nx; }
  
  /* return number of cells in y-direction */
  const unsigned int get_ny() const { return ny; }

  /* return size of domain x-direction */
  const double get_Lx() const { return Lx; }

  /* return size of domain y-direction */
  const double get_Ly() const { return Ly; }

  /* return grid spacing in x-direction */
  const double get_hx() const { return hx; }
  
  /* return grid spacing in y-direction */
  const double get_hy() const { return hy; }

  
  /* get coordinate of center of cell (i,j) */
  const Point get_cell_center(const unsigned int i,
                              const unsigned int j) const {
    return std::make_pair<double,double>((i+0.5)*hx,(j+0.5)*hy);
  }

  /* get coordinate of center of east-facet of cell (i,j) */
  const Point get_east_facet_center(const unsigned int i,
                                    const unsigned int j) const {
    return std::make_pair<double,double>((i+1.0)*hx,(j+0.5)*hy);
  }

  /* get coordinate of center of west-facet of cell (i,j) */
  const Point get_west_facet_center(const unsigned int i,
                                    const unsigned int j) const {
    return std::make_pair<double,double>(i*hx,(j+0.5)*hy);
  }

  /* get coordinate of center of west-facet of cell (i,j) */
  const Point get_north_facet_center(const unsigned int i,
                                     const unsigned int j) const {
    return std::make_pair<double,double>((i+0.5)*hx,(j+1.0)*hy);
  }

  /* get coordinate of center of west-facet of cell (i,j) */
  const Point get_south_facet_center(const unsigned int i,
                                     const unsigned int j) const {
    return std::make_pair<double,double>((i+0.5)*hx,j*hy);
  }

  
  /* return total number of cells */
  const unsigned int size() const { return n; }
  
protected:
  const unsigned int nx; // number of cells in x-direction
  const unsigned int ny; // number of cells in y-direction  
  const unsigned int n; // total number of cells
  const double Lx; // size of domain in x-direction
  const double Ly; // size of domain in y-direction
  const double hx; // grid-spacing in x-direction
  const double hy; // grid-spacing in y-direction
};

#endif // GRID_HH
