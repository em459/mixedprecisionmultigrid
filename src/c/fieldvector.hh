#ifndef VECTOR_HH
#define VECTOR_HH VECTOR_HH
#include <cstddef>
#include <iostream>
#include <algorithm>
#include "config.hh"
#include "grid.hh"

/* ************************************************************* *
   Class for storing field vectors, i.e. data stored on the grid.

   supports basic linear algebra operators such as addition,
   scalar multiplication and calculation of dot-product norm.
   The data is stored internally as a vector on type T, with the 
   x - index running fastest. Data in cell (i,j) is stored at
   position i + nx*j in the vector.
 * ************************************************************** */

template <typename T>
class FieldVector {
public:
  /* construct a new instance */
  FieldVector(const Grid& grid_) : grid(grid_), nx(grid.get_nx()), ny(grid.get_ny()), n(grid_.size()) {
    data = new T[n]; // allocate storage
    std::fill(data,data+n,0.0); // initialise to zero
  }

  /* destructor. free internal storage */
  ~FieldVector() { if (data != NULL) delete[] data; }

  /* return the total length of the vector */
  const unsigned int size() const { return n; }

  /* return data at position (i,j) */
  inline const T get(const unsigned int i, const unsigned int j) const {
    return data[LINIDX(nx,ny,i,j)];
  }

  /* set data at position (i,j) */
  inline void set(const unsigned int i, const unsigned int j, const T value) {
    data[LINIDX(nx,ny,i,j)] = value;
  }

  /* get element using linear indexing */
  inline T& operator[](unsigned int ell) {
    return data[ell];
  }
  
  /* get element using linear indexing (const version( */
  inline const T& operator[](unsigned int ell) const {
    return data[ell];
  }

  /* assign from other: self = other */
  template <class Tother>
  FieldVector<T>& operator=(FieldVector<Tother>& other) {
    std::copy(other.data, other.data+n, data);
    return *this;
  }
    
  /* multiply by scalar: self = alpha*self */
  template <class Tscalar>
  void operator*=(const Tscalar& alpha) {
    std::transform(data,data+n,data, [&alpha](const T x) { return alpha*x; });
  }

  /* add other: self = self + other */
  template <class Tother>
  void operator+=(const FieldVector<Tother>& other) {
    std::transform(data,data+n,other.data,data,
                   [](const T x, const Tother y) {
                     return x+y;
                   });
  }
  
  /* subtract other: self = self - other */
  template <class Tother>
  void operator-=(const FieldVector<Tother>& other) {
    std::transform(data,data+n,other.data,data,
                   [](T x, Tother y) {
                     return x-y;
                   });
  }

  /* binary-add vectors and return new: r = self + other */
  template <class Tother>
  FieldVector<T> operator+(const FieldVector<Tother>& other) const {
    FieldVector<T> r(grid);
    std::transform(data,data+n,other.data,r.data,
                   [](const T x, const Tother y) {
                     return x+y;
                   });
    return r;
  }

  /* binary-subtract vectors and return new: r = self - other */
  template <class Tother>
  FieldVector<T> operator-(const FieldVector<Tother>& other) const {
    FieldVector<T> r(grid);
    std::transform(data,data+n,other.data,r.data,
                   [](const T x, const Tother y) {
                     return x-y;
                   });
    return r;
  }

  /* axpy: self = self + alpha*other */
  template <class Tother, class Tscalar>
  void axpy(const Tscalar alpha, const FieldVector<Tother>& other) {
    std::transform(other.data,other.data+n,data,data,
                   [&alpha](const Tother x, const T y) {
                     return alpha*x+y;
                   });
  }
  
  /* aypx: this = alpha*this + other */
  template <class Tother, class Tscalar>
    void aypx(const Tscalar alpha, const FieldVector<Tother>& other) {
    std::transform(other.data,other.data+n,data,data,
                   [&alpha](const Tother x, const T y) {
                     return x+alpha*y;
                   });
  }

  /* dot with other: return <self,other> */
  template <class Tother>
  const T dot(const FieldVector<Tother>& other) {
    T sum = 0.0;
    for (unsigned int i=0;i<n;++i) {
      sum += data[i]*other.data[i];
    }
    return sum;
  }

  /* Euclidean norm: return ||self||_2 = sqrt(<self,self>) */
  const T two_norm() {
    T sum = 0.0;
    for (unsigned int i=0;i<n;++i) {
      sum += data[i]*data[i];
    }
    return sqrt(sum);
  }

  // ensure that we can access data in fieldvectors of other type
  friend class FieldVector<double>;
  friend class FieldVector<float>;

  T* data; // Internal data storage
  
protected:
  const Grid& grid; // underlying grid
  const unsigned int nx; // Number of cells in x-direction
  const unsigned int ny; // Number of cells in y-direction
  const unsigned int n;  // total number of cells
};

/* Write fieldvector to an output stream */
template <typename T> 
std::ostream& operator<<(std::ostream& os, const FieldVector<T>& v) 
{ 
  os << "["; 
  for (int i = 0; i < v.size(); ++i) { 
    os << v[i]; 
    if (i != v.size() - 1) 
      os << ", "; 
  } 
  os << "]"; 
  return os; 
}

#endif // VECTOR_HH
