program main

! ----------------------------------------------------------------------------
! M A I N program
! ----------------------------------------------------------------------------

  use definitions_mod
  use krylov_mod

  implicit none

  ! Length of vectors
  integer :: n
  ! Vector with right hand side
  real(kind=SOLVER_RL_KIND), dimension(:), allocatable :: u
  ! Vector with solution
  real(kind=SOLVER_RL_KIND), dimension(:), allocatable :: b

  n = 32

  write (*,*) "Precision used in solver         = ", SOLVER_RL_KIND
  write (*,*) "Precision used in preconditioner = ", PREC_RL_KIND

  ! Allocate memory
  allocate(u(n))
  allocate(b(n))
  
  call solve(n,b,u)

  ! Deallocate memory
  deallocate(u)
  deallocate(b)

end program main
