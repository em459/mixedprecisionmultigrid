module preconditioner_mod

! ----------------------------------------------------------------------------
! Module with preconditioners (such as multigrid)
! ----------------------------------------------------------------------------

  use definitions_mod

  implicit none 

contains

  ! Preconditioner for approximately solving the equation A.u = b
  !
  ! Input:
  !  * length of vectors n
  !  * right hand side vector b
  ! Output:
  !  * solution vector u
  subroutine preconditioner(n,b,u)
    implicit none

    ! Passed variables
    integer, intent(in) :: n
    real(kind=PREC_RL_KIND), dimension(n), intent(in) :: b
    real(kind=PREC_RL_KIND), dimension(n), intent(out) :: u

    write(*,*) "Calling preconditioner() with precision", PREC_RL_KIND

    ! >>>>> Implement preconditioner here... <<<<<

  end subroutine preconditioner

end module preconditioner_mod
