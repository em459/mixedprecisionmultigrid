program main

  use datastructures_mod
  use createhelmholtz_mod
  use applyhelmholtz_mod
  use jacobi_mod

  implicit none

  integer :: nx, ny ! Grid size in x- and y-direction
  real(kind=8) :: alpha, beta ! Model parameters
  real(kind=8) :: omega ! Jacobi relaxation parameter
  integer :: niter

  type(FieldType) :: u ! Numerical solution
  type(FieldType) :: u_exact ! Exact solution
  type(FieldType) :: b ! right hand side

  type(MatrixType) :: H ! Helmholtz matrix

  nx = 8
  ny = 8
  alpha = 1.0
  beta = 1.0
  omega = 1.0
  niter = 8

  ! Print out parameters
  write(*,'("nx    = ",I5)') nx
  write(*,'("ny    = ",I5)') ny
  write(*,'("alpha = ",F8.4)') alpha
  write(*,'("beta  = ",F8.4)') beta
  write(*,'("omega = ",F8.4)') omega
  write(*,'("niter = ",I5)') niter
  

  ! Create field with numerical solution
  u%nx = nx
  u%ny = ny
  allocate(u%data(nx,ny))

  ! Create field with exact solution
  u_exact%nx = nx
  u_exact%ny = ny
  allocate(u_exact%data(nx,ny))
  
  ! Create field with RHS
  b%nx = nx
  b%ny = ny
  allocate(b%data(nx,ny))

  ! Create Helmholtz matrix
  H%nx = nx
  H%ny = ny
  allocate(H%data_D(nx,ny))
  allocate(H%data_N(nx,ny))
  allocate(H%data_S(nx,ny))
  allocate(H%data_E(nx,ny))
  allocate(H%data_W(nx,ny))

  ! Populate matrix
  call createH(H,alpha,beta)

  ! Fill u_exact with random values
  call random_number(u_exact%data(:,:))

  ! Apply matrix to get b = H.u_{exact}
  call applyH(H,u_exact,b)

  ! Apply jacobi iteration
  call jacobi(H,b,u,omega,niter)
  
  ! Deallocate memory
  deallocate(u%data)
  deallocate(b%data)
  deallocate(u_exact%data)
  deallocate(H%data_D)
  deallocate(H%data_N)
  deallocate(H%data_S)
  deallocate(H%data_E)
  deallocate(H%data_W)
  
end program main
