module applyhelmholtz_mod

  use datastructures_mod
  
  implicit none

contains

  ! Apply the Helmholtz operator, i.e. calculate v = H.u
  subroutine applyH(H,u,v)
    implicit none

    type(MatrixType), intent(in) :: H ! Helmholtz operator
    type(FieldType), intent(in) :: u ! input field u
    type(FieldType), intent(inout) :: v ! output field v
    integer :: nx, ny ! grid size in x- and y-direction

    nx = H%nx
    ny = H%ny

    ! Apply matrix here...
    
  end subroutine applyH
  
end module applyhelmholtz_mod
